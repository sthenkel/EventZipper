# echo "cleanup EventChecker EventChecker-00-00-00 in /afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtEventCheckertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtEventCheckertempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=EventChecker -version=EventChecker-00-00-00 -path=/afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase  $* >${cmtEventCheckertempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=EventChecker -version=EventChecker-00-00-00 -path=/afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase  $* >${cmtEventCheckertempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtEventCheckertempfile}
  unset cmtEventCheckertempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtEventCheckertempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtEventCheckertempfile}
unset cmtEventCheckertempfile
exit $cmtcleanupstatus


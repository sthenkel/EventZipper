# @file:    EventChecker/python/EventZipperAlg.py
# @purpose: <put some purpose here>
# @author:  <put your name here>

__doc__     = 'some documentation here'
__version__ = '$Revision: 623437 $'
__author__  = '<put your name here>'

import AthenaCommon.SystemOfUnits as Units
import AthenaPython.PyAthena as PyAthena
from AthenaPython.PyAthena import StatusCode

class EventZipperAlg (PyAthena.Alg):
    'put some documentation here'
    def __init__(self, name='EventZipperAlg', **kw):
        ## init base class
        kw['name'] = name
        super(EventZipperAlg, self).__init__(**kw)

        ## properties and data members
        #self.foo = kw.get('foo', 10) # default value
        return

    def initialize(self):
        self.msg.info('==> initialize...')
        return StatusCode.Success

    def execute(self):
        #here's an example of how to loop over something from xAOD
        #for electron in self.evtStore["ElectronCollection"]: print electron.pt()
        
        return StatusCode.Success

    def finalize(self):
        self.msg.info('==> finalize...')
        return StatusCode.Success

    # class EventZipperAlg

#-- start of make_header -----------------

#====================================
#  Library EventChecker
#
#   Generated Tue Feb 16 19:21:53 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventChecker_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventChecker_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventChecker

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventChecker = $(EventChecker_tag)_EventChecker.make
cmt_local_tagfile_EventChecker = $(bin)$(EventChecker_tag)_EventChecker.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventChecker = $(EventChecker_tag).make
cmt_local_tagfile_EventChecker = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventChecker)
#-include $(cmt_local_tagfile_EventChecker)

ifdef cmt_EventChecker_has_target_tag

cmt_final_setup_EventChecker = $(bin)setup_EventChecker.make
cmt_dependencies_in_EventChecker = $(bin)dependencies_EventChecker.in
#cmt_final_setup_EventChecker = $(bin)EventChecker_EventCheckersetup.make
cmt_local_EventChecker_makefile = $(bin)EventChecker.make

else

cmt_final_setup_EventChecker = $(bin)setup.make
cmt_dependencies_in_EventChecker = $(bin)dependencies.in
#cmt_final_setup_EventChecker = $(bin)EventCheckersetup.make
cmt_local_EventChecker_makefile = $(bin)EventChecker.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventChecker :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventChecker'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventChecker/
#EventChecker::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

EventCheckerlibname   = $(bin)$(library_prefix)EventChecker$(library_suffix)
EventCheckerlib       = $(EventCheckerlibname).a
EventCheckerstamp     = $(bin)EventChecker.stamp
EventCheckershstamp   = $(bin)EventChecker.shstamp

EventChecker :: dirs  EventCheckerLIB
	$(echo) "EventChecker ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#EventCheckerLIB :: $(EventCheckerlib) $(EventCheckershstamp)
EventCheckerLIB :: $(EventCheckershstamp)
	$(echo) "EventChecker : library ok"

$(EventCheckerlib) :: $(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(EventCheckerlib) $?
	$(lib_silent) $(ranlib) $(EventCheckerlib)
	$(lib_silent) cat /dev/null >$(EventCheckerstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(EventCheckerlibname).$(shlibsuffix) :: $(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o $(use_requirements) $(EventCheckerstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o $(EventChecker_shlibflags)
	$(lib_silent) cat /dev/null >$(EventCheckerstamp) && \
	  cat /dev/null >$(EventCheckershstamp)

$(EventCheckershstamp) :: $(EventCheckerlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(EventCheckerlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(EventCheckerstamp) && \
	  cat /dev/null >$(EventCheckershstamp) ; fi

EventCheckerclean ::
	$(cleanup_echo) objects EventChecker
	$(cleanup_silent) /bin/rm -f $(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o) $(patsubst %.o,%.dep,$(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o) $(patsubst %.o,%.d.stamp,$(bin)EventZipperAlg.o $(bin)EventChecker_load.o $(bin)EventChecker_entries.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf EventChecker_deps EventChecker_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
EventCheckerinstallname = $(library_prefix)EventChecker$(library_suffix).$(shlibsuffix)

EventChecker :: EventCheckerinstall ;

install :: EventCheckerinstall ;

EventCheckerinstall :: $(install_dir)/$(EventCheckerinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(EventCheckerinstallname) :: $(bin)$(EventCheckerinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(EventCheckerinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##EventCheckerclean :: EventCheckeruninstall

uninstall :: EventCheckeruninstall ;

EventCheckeruninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(EventCheckerinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),EventCheckerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventZipperAlg.d

$(bin)$(binobj)EventZipperAlg.d :

$(bin)$(binobj)EventZipperAlg.o : $(cmt_final_setup_EventChecker)

$(bin)$(binobj)EventZipperAlg.o : $(src)EventZipperAlg.cxx
	$(cpp_echo) $(src)EventZipperAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventZipperAlg_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventZipperAlg_cppflags) $(EventZipperAlg_cxx_cppflags)  $(src)EventZipperAlg.cxx
endif
endif

else
$(bin)EventChecker_dependencies.make : $(EventZipperAlg_cxx_dependencies)

$(bin)EventChecker_dependencies.make : $(src)EventZipperAlg.cxx

$(bin)$(binobj)EventZipperAlg.o : $(EventZipperAlg_cxx_dependencies)
	$(cpp_echo) $(src)EventZipperAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventZipperAlg_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventZipperAlg_cppflags) $(EventZipperAlg_cxx_cppflags)  $(src)EventZipperAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),EventCheckerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventChecker_load.d

$(bin)$(binobj)EventChecker_load.d :

$(bin)$(binobj)EventChecker_load.o : $(cmt_final_setup_EventChecker)

$(bin)$(binobj)EventChecker_load.o : $(src)components/EventChecker_load.cxx
	$(cpp_echo) $(src)components/EventChecker_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventChecker_load_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventChecker_load_cppflags) $(EventChecker_load_cxx_cppflags) -I../src/components $(src)components/EventChecker_load.cxx
endif
endif

else
$(bin)EventChecker_dependencies.make : $(EventChecker_load_cxx_dependencies)

$(bin)EventChecker_dependencies.make : $(src)components/EventChecker_load.cxx

$(bin)$(binobj)EventChecker_load.o : $(EventChecker_load_cxx_dependencies)
	$(cpp_echo) $(src)components/EventChecker_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventChecker_load_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventChecker_load_cppflags) $(EventChecker_load_cxx_cppflags) -I../src/components $(src)components/EventChecker_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),EventCheckerclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventChecker_entries.d

$(bin)$(binobj)EventChecker_entries.d :

$(bin)$(binobj)EventChecker_entries.o : $(cmt_final_setup_EventChecker)

$(bin)$(binobj)EventChecker_entries.o : $(src)components/EventChecker_entries.cxx
	$(cpp_echo) $(src)components/EventChecker_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventChecker_entries_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventChecker_entries_cppflags) $(EventChecker_entries_cxx_cppflags) -I../src/components $(src)components/EventChecker_entries.cxx
endif
endif

else
$(bin)EventChecker_dependencies.make : $(EventChecker_entries_cxx_dependencies)

$(bin)EventChecker_dependencies.make : $(src)components/EventChecker_entries.cxx

$(bin)$(binobj)EventChecker_entries.o : $(EventChecker_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/EventChecker_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(EventChecker_pp_cppflags) $(lib_EventChecker_pp_cppflags) $(EventChecker_entries_pp_cppflags) $(use_cppflags) $(EventChecker_cppflags) $(lib_EventChecker_cppflags) $(EventChecker_entries_cppflags) $(EventChecker_entries_cxx_cppflags) -I../src/components $(src)components/EventChecker_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: EventCheckerclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventChecker.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventCheckerclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library EventChecker
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)EventChecker$(library_suffix).a $(library_prefix)EventChecker$(library_suffix).$(shlibsuffix) EventChecker.stamp EventChecker.shstamp
#-- end of cleanup_library ---------------

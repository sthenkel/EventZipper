#!/python
import os,re,sys

InputData = sys.argv[1]#e.g.: 'Run_periodA.txt'
DataSets = [line.rstrip('\n') for line in open(InputData)]

RegEx = re.compile( '(\w*).(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)' )
UserName = os.environ['USER']

nJobs = 1
FilesPerJob = 20000
Nfiles=90
official = False
UseExternalFile = True
eventwriter = True
eventcollect = False

officialGroup = "perf-idtracking"
officialTag= "--official --voms=atlas:/atlas/"+officialGroup+"/Role=production"
excludeSites = ""

Version = 'EVNTslimmingSortedsma'

athenaVersion = 'AthAnalysisBase,2.4.3'
#athenaVersion = 'AtlasProduction,20.7.3.8'
cmtConfig = "x86_64-slc6-gcc48-opt"
GBperJob = 'MAX'

if(eventwriter):
    print 'INFO :: Event zipping scheduled ...'
    JOs = os.path.expandvars('$TestArea/EventChecker/share/jobOptionsEventWriter.py')
elif (eventcollect):
    print 'INFO :: Event writing scheduled ...'
    JOs = os.path.expandvars('$TestArea/EventChecker/share/jobOptionsEventZipper.py')
else:
    print 'ERROR :: no action specified'
ExternalFile = "exclusionList.txt"
externalFile = os.path.expandvars(ExternalFile)
print 'INFO :: file in-/output : > '+str(externalFile)

outputFile="_EXT0/"

downloadFile = open("Download_"+str(os.path.splitext(InputData)[0])+".sh",'w')
downloadFile.write("#!/bin/bash \n")
for DataSet in DataSets:
    Match = RegEx.match( DataSet )
    if official == True:
        OutputDataSet = 'group.' + officialGroup + '.' + Version + '.' + Match.group( 2 ) + '.' + Match.group( 3 ) +'.' + Match.group( 5 ) + '.' + Match.group( 6 )
        print(OutputDataSet)
    else:
        OutputDataSet = 'user.' + UserName + '.' + Version + '.' + Match.group( 2 ) + '.' + Match.group( 3 ) +'.' + Match.group( 5 ) + '.' + Match.group( 6 )
        print(OutputDataSet)

    proc_file_RDO=DataSet
#    print 'DataSet : '+proc_file_RDO

    downloadFile.write("rucio download "+str(OutputDataSet)+str(outputFile)+" \n")

    if UseExternalFile == True and official == True:
        os.system( 'pathena %s %s  --long --inDS=%s --outDS=%s   --nFilesPerJob=%d --athenaTag=%s --cmtConfig=%s --extFile=%s --addPoolFC=%s --useShortLivedReplicas --excludedSite=%s' % ( JOs, officialTag, DataSet, OutputDataSet, FilesPerJob, athenaVersion,externalFile,externalFile,excludeSites ) )

    if UseExternalFile == True and official == False:
        os.system( 'pathena %s  --long --inDS=%s --outDS=%s  --nFilesPerJob=%d --athenaTag=%s  --extFile=%s --addPoolFC=%s --useShortLivedReplicas --excludedSite=%s' % ( JOs, DataSet, OutputDataSet, FilesPerJob,  athenaVersion,externalFile,externalFile,excludeSites ) )


if UseExternalFile == True and official == True and eventcollect == True:
    os.system( 'pathena %s %s  --long --inDS=%s --outDS=%s   --nFilesPerJob=%d --athenaTag=%s --cmtConfig=%s --extOutFile=%s --useShortLivedReplicas --excludedSite=%s' % ( JOs, officialTag, DataSet, OutputDataSet, FilesPerJob, athenaVersion,externalFile, excludeSites ) )

if UseExternalFile == True and official == False and eventcollect == True:
    os.system( 'pathena %s  --long --inDS=%s --outDS=%s  --nFilesPerJob=%d --athenaTag=%s  --extOutFile=%s --useShortLivedReplicas --excludedSite=%s' % ( JOs, DataSet, OutputDataSet, FilesPerJob,  athenaVersion,externalFile, excludeSites ) )

#-- start of make_header -----------------

#====================================
#  Document EventCheckerConf
#
#   Generated Tue Feb 16 19:22:08 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventCheckerConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventCheckerConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventCheckerConf

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerConf = $(EventChecker_tag)_EventCheckerConf.make
cmt_local_tagfile_EventCheckerConf = $(bin)$(EventChecker_tag)_EventCheckerConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerConf = $(EventChecker_tag).make
cmt_local_tagfile_EventCheckerConf = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventCheckerConf)
#-include $(cmt_local_tagfile_EventCheckerConf)

ifdef cmt_EventCheckerConf_has_target_tag

cmt_final_setup_EventCheckerConf = $(bin)setup_EventCheckerConf.make
cmt_dependencies_in_EventCheckerConf = $(bin)dependencies_EventCheckerConf.in
#cmt_final_setup_EventCheckerConf = $(bin)EventChecker_EventCheckerConfsetup.make
cmt_local_EventCheckerConf_makefile = $(bin)EventCheckerConf.make

else

cmt_final_setup_EventCheckerConf = $(bin)setup.make
cmt_dependencies_in_EventCheckerConf = $(bin)dependencies.in
#cmt_final_setup_EventCheckerConf = $(bin)EventCheckersetup.make
cmt_local_EventCheckerConf_makefile = $(bin)EventCheckerConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventCheckerConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventCheckerConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventCheckerConf/
#EventCheckerConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: EventCheckerConf EventCheckerConfclean

confpy  := EventCheckerConf.py
conflib := $(bin)$(library_prefix)EventChecker.$(shlibsuffix)
confdb  := EventChecker.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

EventCheckerConf :: EventCheckerConfinstall

install :: EventCheckerConfinstall

EventCheckerConfinstall : /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker/$(confpy)
	@echo "Installing /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker in /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/InstallArea/python ; \

/afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker/$(confpy) : $(conflib) /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)EventChecker.$(shlibsuffix)

/afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker:
	@ if [ ! -d /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker ] ; then mkdir -p /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker ; fi ;

EventCheckerConfclean :: EventCheckerConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker/$(confpy) /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker/$(confdb)

uninstall :: EventCheckerConfuninstall

EventCheckerConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/InstallArea/python
libEventChecker_so_dependencies = ../x86_64-slc6-gcc48-opt/libEventChecker.so
#-- start of cleanup_header --------------

clean :: EventCheckerConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventCheckerConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventCheckerConfclean ::
#-- end of cleanup_header ---------------

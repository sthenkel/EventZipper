#-- start of make_header -----------------

#====================================
#  Document EventChecker_python_init
#
#   Generated Tue Feb 16 19:22:10 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventChecker_python_init_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventChecker_python_init_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventChecker_python_init

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventChecker_python_init = $(EventChecker_tag)_EventChecker_python_init.make
cmt_local_tagfile_EventChecker_python_init = $(bin)$(EventChecker_tag)_EventChecker_python_init.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventChecker_python_init = $(EventChecker_tag).make
cmt_local_tagfile_EventChecker_python_init = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventChecker_python_init)
#-include $(cmt_local_tagfile_EventChecker_python_init)

ifdef cmt_EventChecker_python_init_has_target_tag

cmt_final_setup_EventChecker_python_init = $(bin)setup_EventChecker_python_init.make
cmt_dependencies_in_EventChecker_python_init = $(bin)dependencies_EventChecker_python_init.in
#cmt_final_setup_EventChecker_python_init = $(bin)EventChecker_EventChecker_python_initsetup.make
cmt_local_EventChecker_python_init_makefile = $(bin)EventChecker_python_init.make

else

cmt_final_setup_EventChecker_python_init = $(bin)setup.make
cmt_dependencies_in_EventChecker_python_init = $(bin)dependencies.in
#cmt_final_setup_EventChecker_python_init = $(bin)EventCheckersetup.make
cmt_local_EventChecker_python_init_makefile = $(bin)EventChecker_python_init.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventChecker_python_init :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventChecker_python_init'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventChecker_python_init/
#EventChecker_python_init::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of python_init_header ------

installarea = ${CMTINSTALLAREA}$(shared_install_subdir)
install_python_dir = $(installarea)/python/EventChecker
init_file = $(install_python_dir)/__init__.py


EventChecker_python_init :: EventChecker_python_initinstall

install :: EventChecker_python_initinstall

EventChecker_python_initinstall :: $(init_file)

$(init_file) ::
	@if [ -e $(install_python_dir) -a ! -e $(init_file) ]; then \
	  echo "Installing __init__.py file from ${GAUDIPOLICYROOT}" ; \
	  $(install_command) ${GAUDIPOLICYROOT}/cmt/fragments/__init__.py $(install_python_dir) ; \
	fi

EventChecker_python_initclean :: EventChecker_python_inituninstall

uninstall :: EventChecker_python_inituninstall

EventChecker_python_inituninstall ::
	@$(uninstall_command) $(init_file)


#-- end of python_init_header ------
#-- start of cleanup_header --------------

clean :: EventChecker_python_initclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventChecker_python_init.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventChecker_python_initclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Document EventCheckerComponentsList
#
#   Generated Tue Feb 16 19:22:10 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventCheckerComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventCheckerComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventCheckerComponentsList

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerComponentsList = $(EventChecker_tag)_EventCheckerComponentsList.make
cmt_local_tagfile_EventCheckerComponentsList = $(bin)$(EventChecker_tag)_EventCheckerComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerComponentsList = $(EventChecker_tag).make
cmt_local_tagfile_EventCheckerComponentsList = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventCheckerComponentsList)
#-include $(cmt_local_tagfile_EventCheckerComponentsList)

ifdef cmt_EventCheckerComponentsList_has_target_tag

cmt_final_setup_EventCheckerComponentsList = $(bin)setup_EventCheckerComponentsList.make
cmt_dependencies_in_EventCheckerComponentsList = $(bin)dependencies_EventCheckerComponentsList.in
#cmt_final_setup_EventCheckerComponentsList = $(bin)EventChecker_EventCheckerComponentsListsetup.make
cmt_local_EventCheckerComponentsList_makefile = $(bin)EventCheckerComponentsList.make

else

cmt_final_setup_EventCheckerComponentsList = $(bin)setup.make
cmt_dependencies_in_EventCheckerComponentsList = $(bin)dependencies.in
#cmt_final_setup_EventCheckerComponentsList = $(bin)EventCheckersetup.make
cmt_local_EventCheckerComponentsList_makefile = $(bin)EventCheckerComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventCheckerComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventCheckerComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventCheckerComponentsList/
#EventCheckerComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = EventChecker.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libEventChecker.$(shlibsuffix)

EventCheckerComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: EventCheckerComponentsListinstall
EventCheckerComponentsListinstall :: EventCheckerComponentsList

uninstall :: EventCheckerComponentsListuninstall
EventCheckerComponentsListuninstall :: EventCheckerComponentsListclean

EventCheckerComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: EventCheckerComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventCheckerComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventCheckerComponentsListclean ::
#-- end of cleanup_header ---------------

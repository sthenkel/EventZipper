// EventChecker includes
#include "EventZipperAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"


#include "PersistentDataModel/DataHeader.h"


//uncomment the line below to use the HistSvc for outputting trees and histograms
//#include "GaudiKernel/ITHistSvc.h"
//#include "TTree.h"
//#include "TH1D.h"

#include <string>

EventZipperAlg::EventZipperAlg( const std::string& name,
				ISvcLocator* pSvcLocator )
  : AthAnalysisAlgorithm( name, pSvcLocator ),
    m_nentries(0),
    m_evttype("StreamAOD")

{

  declareProperty( "fileEntries", m_nentries );
  declareProperty( "evtType", m_evttype ); 

}


EventZipperAlg::~EventZipperAlg() {}


StatusCode EventZipperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  
  ATH_MSG_DEBUG("Retrieving event info.");
  ATH_MSG_INFO("Number of entries in file: " << m_nentries << "");

  m_counter = 0;
  m_exclusionList = "exclusionList.txt";
  ATH_MSG_INFO("Start writing out file containing evt numbers to zip : " << m_exclusionList << "");
  m_outfile.open (m_exclusionList);
  return StatusCode::SUCCESS;
}

StatusCode EventZipperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //
  m_outfile.close();

  return StatusCode::SUCCESS;
}

StatusCode EventZipperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  m_counter++;
  setFilterPassed(false); //optional: start with algorithm not passed

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //  const xAOD::EventInfo* evtInfo = 0;
  // CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
  // ATH_MSG_INFO("eventNumber=" << evtInfo->eventNumber() );
  // ATH_MSG_INFO("mcEventNumber=" << evtInfo->mcEventNumber() );
  
  const EventInfo * eventInfo;
  CHECK( evtStore()->retrieve(eventInfo) );

  //  ATH_MSG_INFO("Event Number ("<< eventInfo->event_ID()->event_number()<< ")");
 
  
  const DataHeader* dh = 0;
  CHECK( evtStore()->retrieve(dh) );

  for(auto itr = dh->beginProvenance(); itr != dh->endProvenance(); ++itr) {
    //    std::cout << itr->getKey() << " : " << itr->getToken()->dbID().toString() << std::endl;
    if((itr->getKey()).compare(m_evttype) == 0) {
      ATH_MSG_INFO("("<<m_counter<<") "<< itr->getKey()   << "  "
		<< eventInfo->event_ID()->run_number()    << "  "
		<< eventInfo->event_ID()->event_number()  << "  "
		    << itr->getToken()->dbID().toString() );
      if(m_nentries-m_counter != 0){
	m_outfile << eventInfo->event_ID()->event_number() <<",";
      }else{
	m_outfile << eventInfo->event_ID()->event_number();
      }
    }// end evttype comparison
  }
  
  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode EventZipperAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //
  ATH_MSG_INFO ("beginInputFile() :: initializing");
  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  //  const xAOD::CutBookkeeperContainer* bks = 0;
  //  CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
  //  for(auto cbk : *bks){
  //    if(cbk->inputStream()=="StreamAOD") std::cout << "hallo"<< std::endl;
  //       }
  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  std::cout << "Beam energy : "<< beamEnergy<< std::endl;
  
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}



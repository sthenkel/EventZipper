# echo "cleanup EventChecker EventChecker-00-00-00 in /afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtEventCheckertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtEventCheckertempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=EventChecker -version=EventChecker-00-00-00 -path=/afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase  $* >${cmtEventCheckertempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=EventChecker -version=EventChecker-00-00-00 -path=/afs/cern.ch/work/s/sthenkel/public/forPAT/AthAnalysisBase  $* >${cmtEventCheckertempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtEventCheckertempfile}
  unset cmtEventCheckertempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtEventCheckertempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtEventCheckertempfile}
unset cmtEventCheckertempfile
return $cmtcleanupstatus



#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../EventZipperAlg.h"

DECLARE_ALGORITHM_FACTORY( EventZipperAlg )

DECLARE_FACTORY_ENTRIES( EventChecker )
{
  DECLARE_ALGORITHM( EventZipperAlg );
}

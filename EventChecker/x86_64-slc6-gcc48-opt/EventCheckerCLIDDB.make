#-- start of make_header -----------------

#====================================
#  Document EventCheckerCLIDDB
#
#   Generated Tue Feb 16 19:22:08 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventCheckerCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventCheckerCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventCheckerCLIDDB

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerCLIDDB = $(EventChecker_tag)_EventCheckerCLIDDB.make
cmt_local_tagfile_EventCheckerCLIDDB = $(bin)$(EventChecker_tag)_EventCheckerCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerCLIDDB = $(EventChecker_tag).make
cmt_local_tagfile_EventCheckerCLIDDB = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventCheckerCLIDDB)
#-include $(cmt_local_tagfile_EventCheckerCLIDDB)

ifdef cmt_EventCheckerCLIDDB_has_target_tag

cmt_final_setup_EventCheckerCLIDDB = $(bin)setup_EventCheckerCLIDDB.make
cmt_dependencies_in_EventCheckerCLIDDB = $(bin)dependencies_EventCheckerCLIDDB.in
#cmt_final_setup_EventCheckerCLIDDB = $(bin)EventChecker_EventCheckerCLIDDBsetup.make
cmt_local_EventCheckerCLIDDB_makefile = $(bin)EventCheckerCLIDDB.make

else

cmt_final_setup_EventCheckerCLIDDB = $(bin)setup.make
cmt_dependencies_in_EventCheckerCLIDDB = $(bin)dependencies.in
#cmt_final_setup_EventCheckerCLIDDB = $(bin)EventCheckersetup.make
cmt_local_EventCheckerCLIDDB_makefile = $(bin)EventCheckerCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventCheckerCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventCheckerCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventCheckerCLIDDB/
#EventCheckerCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: EventCheckerCLIDDB EventCheckerCLIDDBclean

outname := clid.db
cliddb  := EventChecker_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)EventChecker.$(shlibsuffix)

EventCheckerCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p EventChecker -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

EventCheckerCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: EventCheckerCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventCheckerCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventCheckerCLIDDBclean ::
#-- end of cleanup_header ---------------

#-- start of make_header -----------------

#====================================
#  Document EventCheckerConfDbMerge
#
#   Generated Tue Feb 16 19:22:10 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_EventCheckerConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_EventCheckerConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_EventCheckerConfDbMerge

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerConfDbMerge = $(EventChecker_tag)_EventCheckerConfDbMerge.make
cmt_local_tagfile_EventCheckerConfDbMerge = $(bin)$(EventChecker_tag)_EventCheckerConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

EventChecker_tag = $(tag)

#cmt_local_tagfile_EventCheckerConfDbMerge = $(EventChecker_tag).make
cmt_local_tagfile_EventCheckerConfDbMerge = $(bin)$(EventChecker_tag).make

endif

include $(cmt_local_tagfile_EventCheckerConfDbMerge)
#-include $(cmt_local_tagfile_EventCheckerConfDbMerge)

ifdef cmt_EventCheckerConfDbMerge_has_target_tag

cmt_final_setup_EventCheckerConfDbMerge = $(bin)setup_EventCheckerConfDbMerge.make
cmt_dependencies_in_EventCheckerConfDbMerge = $(bin)dependencies_EventCheckerConfDbMerge.in
#cmt_final_setup_EventCheckerConfDbMerge = $(bin)EventChecker_EventCheckerConfDbMergesetup.make
cmt_local_EventCheckerConfDbMerge_makefile = $(bin)EventCheckerConfDbMerge.make

else

cmt_final_setup_EventCheckerConfDbMerge = $(bin)setup.make
cmt_dependencies_in_EventCheckerConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_EventCheckerConfDbMerge = $(bin)EventCheckersetup.make
cmt_local_EventCheckerConfDbMerge_makefile = $(bin)EventCheckerConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)EventCheckersetup.make

#EventCheckerConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'EventCheckerConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = EventCheckerConfDbMerge/
#EventCheckerConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: EventCheckerConfDbMerge EventCheckerConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/AthAnalysisBase/EventChecker/genConf/EventChecker/EventChecker.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

EventCheckerConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  EventCheckerConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

EventCheckerConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libEventChecker_so_dependencies = ../x86_64-slc6-gcc48-opt/libEventChecker.so
#-- start of cleanup_header --------------

clean :: EventCheckerConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(EventCheckerConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

EventCheckerConfDbMergeclean ::
#-- end of cleanup_header ---------------

#!/bin/python

import glob,os
fileDir = "."
files = glob.glob(fileDir+'/*List.txt')
os.system("mkdir -p output")
for ex_file in files:
    print 'INFO :: sorting file ... > ' + str( ex_file )
    InputExclusionEvents = open(fileDir+"/"+ex_file, 'r')
    exclusionlist = sorted( InputExclusionEvents.read().split(","), key=int)
    print exclusionlist[1], len(exclusionlist)
    useList = ""
    for evtNumber in range( len(exclusionlist) ):
        if evtNumber == 0:
            useList += "[" + str(exclusionlist[evtNumber]) + ","
        elif evtNumber == len(exclusionlist)-1:
            useList += str(exclusionlist[evtNumber]) + "]"
        else:
            useList += str(exclusionlist[evtNumber]) + ","
#    print 'EXCLUSIONLIST : ' + str(useList)

    outputfile = open(fileDir+"/output/sorted_"+ex_file[2:], 'w')
    outputfile.write( useList )

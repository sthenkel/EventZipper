#ifndef EVENTCHECKER_EVENTZIPPERALG_H
#define EVENTCHECKER_EVENTZIPPERALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

#include <iostream>
#include <fstream>



class EventZipperAlg: public ::AthAnalysisAlgorithm { 
 public: 
  EventZipperAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~EventZipperAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
  
  virtual StatusCode beginInputFile();

 private: 

  int m_nentries;
  std::string m_evttype;
  int m_counter;
 
  std::string m_exclusionList;
  std::ofstream m_outfile;
}; 

#endif //> !EVENTCHECKER_EVENTZIPPERALG_H
